import requests
import sqlite3
from datetime import datetime


def get_exchange_rates(currency_code):
    url = 'https://api.monobank.ua/bank/currency'
    params = {'currencyCodeA': currency_code, 'currencyCodeB': 840}
    response = requests.get(url, params=params)
    data = response.json()

    print(f"API Response: {data}")

    if data and isinstance(data, list) and len(data) > 0:
        return data
    else:
        return None


def calculate_average_rate(rate_sell, rate_buy):
    return (rate_sell + rate_buy) / 2


def insert_into_db(connection, cursor, currency_name, currency_value, current_date):
    cursor.execute("INSERT INTO ExchangeRateToUSD (currency_name, currency_value, current_date) VALUES (?, ?, ?)",
                   (currency_name, currency_value, current_date))
    connection.commit()


connection = sqlite3.connect('../data/exchange_rates_db')
cursor = connection.cursor()

cursor.execute('''CREATE TABLE IF NOT EXISTS ExchangeRateToUSD (
                  id INTEGER PRIMARY KEY,
                  currency_name TEXT,
                  currency_value REAL,
                  current_date DATETIME)''')

currency_codes = ['980', '978', '826', '392', '756']
for currency_code in currency_codes:
    exchange_rates = get_exchange_rates(currency_code)

    if exchange_rates is not None:
        current_date = datetime.now().strftime('%m/%d/%Y %I:%M %p')

        for rate_info in exchange_rates:
            currency_name = str(rate_info['currencyCodeA'])
            rate_sell = rate_info.get('rateSell', 0)
            rate_buy = rate_info.get('rateBuy', 0)

            if rate_sell != 0 and rate_buy != 0:
                currency_value = calculate_average_rate(rate_sell, rate_buy)
                insert_into_db(connection, cursor, currency_name, currency_value, current_date)

connection.close()
