import requests
import sqlite3
from datetime import datetime

def get_exchange_rates():
    url = 'https://api.monobank.ua/bank/currency'
    response = requests.get(url)
    data = response.json()

    print(f"API Response: {data}")  # Добавлен вывод для отладки

    if data and isinstance(data, list) and len(data) > 0:
        return data
    else:
        return None

def insert_into_db(connection, cursor, currency_name, currency_value, current_date):
    cursor.execute("INSERT INTO ExchangeRateToUSD (currency_name, currency_value, current_date) VALUES (?, ?, ?)",
                   (currency_name, currency_value, current_date))
    connection.commit()

connection = sqlite3.connect('../data/exchange_rates_db')
cursor = connection.cursor()

cursor.execute('''CREATE TABLE IF NOT EXISTS ExchangeRateToUSD (
                  id INTEGER PRIMARY KEY,
                  currency_name TEXT,
                  currency_value REAL,
                  current_date DATETIME)''')

# Получаем все курсы обмена
exchange_rates = get_exchange_rates()

if exchange_rates is not None:
    current_date = datetime.now().strftime('%m/%d/%Y %I:%M %p')

    for rate_info in exchange_rates:
        currency_name = str(rate_info['currencyCodeA'])
        currency_value = rate_info['rateCross']
        insert_into_db(connection, cursor, currency_name, currency_value, current_date)

connection.close()
