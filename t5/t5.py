import requests
import sqlite3
from datetime import datetime


def get_exchange_rate(currency_code):
    url = f'https://api.monobank.ua/bank/currency'
    params = {'currencyCodeA': currency_code, 'currencyCodeB': 840}  # according to iso 4217 USD code is 840
    response = requests.get(url, params=params)
    data = response.json()

    if data:
        return data[0]['rateCross']
    else:
        return None


def insert_into_db(connection, cursor, currency_name, currency_value, current_date):
    cursor.execute("INSERT INTO ExchangeRateToUSD (currency_name, currency_value, current_date) VALUES (?, ?, ?)",
                   (currency_name, currency_value, current_date))
    connection.commit()


connection = sqlite3.connect('../data/exchange_rates_db')
cursor = connection.cursor()

cursor.execute('''CREATE TABLE IF NOT EXISTS ExchangeRateToUSD (
                  id INTEGER PRIMARY KEY,
                  currency_name TEXT,
                  currency_value REAL,
                  current_date DATETIME)''')

currency_codes = ['980', '978', '826', '392', '756']

for currency_code in currency_codes:
    rate = get_exchange_rate(currency_code)
    current_date = datetime.now().strftime('%m/%d/%Y %I:%M %p')
    if rate is not None:
        insert_into_db(connection, cursor, currency_code, rate, current_date)

connection.close()
