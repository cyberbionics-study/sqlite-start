from t1.t1 import create_connection, execute_query, execute_read_query
from datetime import datetime
# here i had few ways of solving the task of adding info to DB, i picked adding new table because understand this way
# better and it needed less research while result passable.
if __name__ == '__main__':
    try:
        connection = create_connection("../data/sqliteDB")
        create_incomes_table = """
        CREATE TABLE IF NOT EXISTS incomes (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          amount INTEGER NOT NULL,
          time DATETIME,
          source TEXT
        );
        """
        execute_query(connection, create_incomes_table)

        insert_incomes = """
            INSERT INTO incomes (amount, time, source) VALUES (?, ?, ?);
            """
        data = ((2000, datetime(2023, 11, 2, 15, 30), 'payment for invoice 20230925'),
                (150, datetime(2023, 11, 3, 0, 17), 'casino profit'))
        for element in data:
            execute_query(connection, insert_incomes, element)

        select_all = "SELECT * FROM incomes;"
        result = execute_read_query(connection, select_all)
        print(result)

    except Exception as e:
        print(f"Error: {e}")
    finally:
        if connection:
            connection.close()
