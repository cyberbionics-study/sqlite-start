from t1.t1 import execute_query, create_connection
from datetime import datetime


def add_new_spend(conn):
    """this is updated function from t2 for adding info to DB from keyboard"""
    table = input('please chose table: ')
    if table == 'spends':
        amount_ins = int(input('please input amount: '))
        time_ins_str = input('Please input full time (YYYY-MM-DD HH:MM): ')
        add_ins = input('please input target: ')
        insert = """
                INSERT INTO spends (amount, time, target) VALUES (?, ?, ?);
                """
    elif table == 'incomes':
        amount_ins = int(input('please input amount: '))
        time_ins_str = input('Please input full time (YYYY-MM-DD HH:MM): ')
        add_ins = input('please input source: ')
        insert = """
                INSERT INTO incomes (amount, time, source) VALUES (?, ?, ?);
                """
    else:
        print('unknown command')

    time_ins_str = time_ins_str.strip()
    time_ins = datetime.strptime(time_ins_str, "%Y-%m-%d %H:%M")
    data = (amount_ins, time_ins, add_ins)

    execute_query(conn, insert, data)


if __name__ == '__main__':
    connection = create_connection("../data/sqliteDB")
    add_new_spend(connection)
