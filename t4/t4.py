from t1.t1 import create_connection, execute_read_query
from datetime import datetime


# two functions below can be designed as one function. that solution will better match with DRY rule
# I decided not to do this in the interests of my subjective ability to read the code
def calculate_total_spends(conn, month: datetime.month = None):
    """function for calculating summary amount from spends table for certain month or all time
    -------------------------------------------------------------------------------
    function works not totally correct when DB has records for more than one year (it will count summary for all
    septembers if you give arg month='9' for example)
    fix of this problem makes code more complicated and not necessary for this task, so i decided not to do this """
    query = "SELECT amount, strftime('%m', time) AS month FROM spends"
    res = execute_read_query(conn=conn, query=query)
    summ = 0
    if month:
        for item in res:
            if item[1] == month:
                summ += item[0]
    else:
        for item in res:
            summ += item[0]
    return summ


def calculate_total_incomes(conn, month: datetime.month = None):
    """function for calculating summary amount from incomes table for certain month or all time
    -------------------------------------------------------------------------------
    function works not totally correct when DB has records for more than one year (it will count summary for all
    septembers if you give arg month='9' for example)
    fix of this problem makes code more complicated and not necessary for this task, so i decided not to do this """

    query = "SELECT amount, strftime('%m', time ) AS month FROM incomes"
    res = execute_read_query(conn=conn, query=query)
    summ = 0
    if month:
        for item in res:
            if item[1] == month:
                summ += item[0]
    else:
        for item in res:
            summ += item[0]
    return summ


def some_menu(conn):
    """some menu prototype which can be updated and used for more advanced UI
    could do it with TK, decided not to waste time"""
    choice = input('please chose what summary you want to get: ')
    if choice in ('+', 'income', 'incomes'):
        mon = input('please pick month (if not picked will give summary for all time): ')
        if mon is not None or int(mon) not in range(13):
            print('incorrect month input')
        res = calculate_total_incomes(conn=conn, month=mon)
        print(res)
    elif choice in ('-', 'spend', 'spends'):
        mon = input('please pick month (if not picked will give summary for all time): ')
        if mon is not None or int(mon) not in range(13):
            print('incorrect month input')
        res = calculate_total_spends(conn=conn, month=mon)
        print(res)
    else:
        print('unknown command')


if __name__ == '__main__':
    connection = create_connection("../data/sqliteDB")
    some_menu(connection)
