import sqlite3
from sqlite3 import Error
from datetime import datetime


def create_connection(path):
    """connect creating function"""
    conn = None
    try:
        conn = sqlite3.connect(path)
        print("Connection to SQLite DB successful")
    except Error as e:
        print(f"The error '{e}' occurred")

    return conn


def execute_query(conn, query, data=None):
    """function that executes command to SQLite which is contained in a string
    optional argument data is used when you need to paste some python variables or functions into query"""
    cursor = conn.cursor()
    try:
        if data:
            cursor.execute(query, data)
        else:
            cursor.execute(query)

        conn.commit()
        print("Query executed successfully")
    except Error as e:
        print(f"The error '{e}' occurred")


def execute_read_query(conn, query):            # perfectly should be updated to be able using optional data arg too,
    """function for read requests to SQLite"""  # but not required feat in this tasks set
    cursor = conn.cursor()
    result = None
    try:
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    except Error as e:
        print(f"Executing request error: {e}")
    finally:
        if cursor:
            cursor.close()


if __name__ == '__main__':
    connection = create_connection("../data/sqliteDB")
    create_spends_table = """
    CREATE TABLE IF NOT EXISTS spends (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      amount INTEGER NOT NULL,
      time DATETIME,
      target TEXT
    );
    """
    execute_query(connection, create_spends_table)

    insert_spends = """
    INSERT INTO spends (amount, time, target) VALUES (?, ?, ?);
    """
    data = ((200, datetime(2023, 11, 2, 15), 'groceries'),
            (15, datetime(2023, 11, 3, 0, 15), 'cigarettes'),
            (300, datetime(2023, 11, 3, 0, 20), 'gas'))
    for element in data:
        execute_query(connection, insert_spends, element)
