from t1.t1 import execute_query, create_connection
from datetime import datetime


def add_new_spend(conn):
    """function for adding data to DB from keyboard"""
    amount_ins = int(input('please input amount: '))
    time_ins_str = input('Please input full time (YYYY-MM-DD HH:MM): ')  # this is the least ugly solution of str
    time_ins_str = time_ins_str.strip()                                  # datetime to proper datetime i found
    time_ins = datetime.strptime(time_ins_str, "%Y-%m-%d %H:%M")
    target_ins = input('please input target: ')

    insert = """
        INSERT INTO spends (amount, time, target) VALUES (?, ?, ?);
        """
    data = (amount_ins, time_ins, target_ins)

    execute_query(conn, insert, data)


if __name__ == '__main__':
    connection = create_connection("../data/sqliteDB")
    add_new_spend(connection)
